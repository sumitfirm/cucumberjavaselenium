package StepsDefinations;

import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {


	@Given("browser is open")
	public void browser_is_open() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("browser is open");
		
	}

	@Given("user is on login page")
	public void user_is_on_login_page() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("user is on login page");
		Assert.assertEquals(true, true);
		
	}

	@When("^user enters (.*) and (.*)$")
	public void user_enters_id_and_Password(String id, String pass) {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("user enters" +  id + "  and "+   pass);
		Assert.assertEquals(true, true);
	}

	@When("user clicks on login")
	public void user_clicks_on_login() {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("user clicks on login");
		Assert.assertEquals(true, false);
	
	}

	@Then("user is navigated to the home page")
	public void user_is_navigated_to_the_home_page() {
		// Write code here that turns the phrase above into concrete actions
	}

	@When("user enters Ele and {int}")
	public void user_enters_ele_and(Integer int1) {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("user is navigated to the home page");
		
	}


}
