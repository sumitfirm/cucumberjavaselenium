package Runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import utils.ExtentManager;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions
			(
			features="src/test/resources/features", 
			glue= {"StepsDefinations"},
			monochrome=true,
			plugin = {
					"pretty"
					//,"html:target/cucumber-report-html/report.html"
					,"json:target/cucumber.json"
					,"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"

					}
			)
public class TestRunner {
	
	@BeforeClass
    public static void setup() {
        ExtentManager.getInstance();
    }

    @AfterClass
    public static void teardown() {
        ExtentManager.getInstance().flush();
    }

}